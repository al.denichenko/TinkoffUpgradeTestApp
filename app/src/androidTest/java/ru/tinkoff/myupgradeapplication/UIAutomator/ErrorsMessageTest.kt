package ru.tinkoff.myupgradeapplication.UIAutomator

import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import ru.tinkoff.myupgradeapplication.MainActivity

@RunWith(AndroidJUnit4::class)
class ErrorsMessageTest {
    @get:Rule
    private val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Before
    fun openSecondPage() {
        with(FirstPage()) {
            pressNextButton()
        }
    }

    @After
    fun clearData() {
        with(LoginPage()) {
            clearLogin()
            clearPassword()
        }
    }

    @Test
    fun checkEmptyPassword() {
        with(LoginPage()) {
            enterLogin("123")
            pressSubmitButton()
            checkTextOnSnackbar("Password field must be filled!")
        }
    }

    @Test
    fun checkEmptyLogin() {
        with(LoginPage()) {
            enterPassword("123")
            pressSubmitButton()
            checkTextOnSnackbar("Login field must be filled!")
        }
    }

    @Test
    fun checkEmptyPasswordAndEmptyLogin() {
        with(LoginPage()) {
            pressSubmitButton()
            checkTextOnSnackbar("Both of fields must be filled!")
        }
    }
}