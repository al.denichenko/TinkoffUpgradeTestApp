package ru.tinkoff.myupgradeapplication.UIAutomator

import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import io.github.kakaocup.kakao.common.utilities.getResourceString
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import ru.tinkoff.myupgradeapplication.MainActivity
import ru.tinkoff.myupgradeapplication.R

@RunWith(AndroidJUnit4::class)
class DialogWindowTest {
    @get:Rule
    private val activityRule = ActivityScenarioRule(MainActivity::class.java)

    private val text = getResourceString(R.string.first_text)

    @Before
    fun openDialog() {
        with(FirstPage()) {
            pressShowDialog()
        }
    }

    @After
    fun closeDialog() {
        with(FirstPage()) {
            pressBack()
        }
    }

    @Test
    fun showDialogTest() {
        with(FirstPage()) {
            checkTextOnSnackbar("Важное сообщение Теперь ты автоматизатор")
        }
    }

    @Test
    fun closeDialogTest() {
        with(FirstPage()) {
            pressBack()
            checkTextOnPage(text)
        }
    }
}