package ru.tinkoff.myupgradeapplication.UIAutomator

import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.By
import androidx.test.uiautomator.UiDevice
import androidx.test.uiautomator.Until
import junit.framework.TestCase.assertEquals

class FirstPage {
    private val device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
    private val waitingTimeOut = 2000L
    private val nextButton = By.res("button_first")
    private val showDialogButton = By.res("dialog_button")
    private val changeButton = By.res("change_button")
    private val textOnFirstPage = By.res("ru.tinkoff.myupgradeapplication:id/textview_first")

    fun pressNextButton() {
        device.wait(
            Until.findObject(nextButton),
            waitingTimeOut
        ).click()
    }

    fun pressShowDialog() {
        device.wait(
            Until.findObject(showDialogButton),
            waitingTimeOut
        ).click()
    }

    fun checkTextOnSnackbar(text: String) {
        device
            .wait(Until.hasObject(By.text(text)), waitingTimeOut)
    }

    fun pressBack() {
        device.pressBack()
    }

    fun pressChangeButton() {
        device.wait(
            Until.findObject(changeButton),
            waitingTimeOut
        ).click()
    }

    fun checkTextOnPage(getText: String) {
        val text = device.wait(
            Until.findObject(textOnFirstPage),
            waitingTimeOut
        ).text

        assertEquals("", text, getText)
    }
}
