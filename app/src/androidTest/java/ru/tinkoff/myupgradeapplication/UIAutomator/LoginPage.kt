package ru.tinkoff.myupgradeapplication.UIAutomator

import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.By
import androidx.test.uiautomator.UiDevice
import androidx.test.uiautomator.Until
import junit.framework.TestCase.assertEquals

class LoginPage {
    private val device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
    private val waitingTimeOut = 2000L
    private val loginField = By.res("ru.tinkoff.myupgradeapplication:id/edittext_login")
    private val passwordField = By.res("ru.tinkoff.myupgradeapplication:id/edittext_password")
    private val submitButton = By.res("button_submit")
    private val previousButton = By.res("button_second")

    fun enterLogin(login: String) {
        device.wait(
            Until.findObject(loginField),
            waitingTimeOut
        ).text = login
    }

    fun enterPassword(password: String) {
        device.wait(
            Until.findObject(passwordField),
            waitingTimeOut
        ).text = password
    }

    fun pressSubmitButton() {
        device.wait(
            Until.findObject(submitButton),
            waitingTimeOut
        ).click()
    }

    fun pressPreviousButton() {
        device.wait(
            Until.findObject(previousButton),
            waitingTimeOut
        ).click()
    }

    fun checkTextOnSnackbar(text: String) {
        device
            .wait(Until.hasObject(By.text(text)), waitingTimeOut)
    }

    fun clearLogin() {
        device.wait(
            Until.findObject(loginField),
            waitingTimeOut
        ).text = ""
    }

    fun clearPassword() {
        device.wait(
            Until.findObject(passwordField),
            waitingTimeOut
        ).text = ""
    }

    fun checkDefaultLoginAndPassword() {
        val login = device.wait(Until.findObject(loginField), waitingTimeOut).text
        val password = device.wait(Until.findObject(passwordField), waitingTimeOut).text
        assertEquals("", "Login", login)
        assertEquals("", "Password", password)
    }
}
