package ru.tinkoff.myupgradeapplication.UIAutomator

import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import io.github.kakaocup.kakao.common.utilities.getResourceString
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import ru.tinkoff.myupgradeapplication.MainActivity
import ru.tinkoff.myupgradeapplication.R

@RunWith(AndroidJUnit4::class)
class NavigationTest {
    @get:Rule
    private val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun checkDefaultText() {
        val text = getResourceString(R.string.first_text)

        with(FirstPage()) {
            pressChangeButton()
            pressNextButton()
        }

        with(LoginPage()) {
            pressPreviousButton()
        }

        with(FirstPage()) {
            checkTextOnPage(text)
        }
    }

    @Test
    fun checkDefaultLoginAndPasswordTest() {
        with(FirstPage()) {
            pressNextButton()
        }

        with(LoginPage()) {
            enterLogin("123")
            enterPassword("123")
            pressPreviousButton()
        }

        with(FirstPage()) {
            pressNextButton()
        }

        with(LoginPage()) {
            checkDefaultLoginAndPassword()
        }
    }
}